import AbtractsPage from "../commons/AbtractsPage";

class AddAddressPage extends AbtractsPage {

  constructor(){
    super();
    this.profileName = "//span[@class='js-name']";
    this.addressPopup = "//a[contains(text(),'Addresses')]";
    this.addNewAddressBtn = "//a[@class='btn btn-xs btnTextOnly gold addNewAddress js-showAddressBox']";
    this.fullNameInput = "//input[@id='address_name']";
    this.addressInput = "//input[@id='address_Line1']";
    this.postalCodeInput = "//input[@id='address_PostalCode']";
    this.phoneNumberInput = "//input[@id='TelephoneNumber']";
    this.addAddressBtn = "//input[@id='addressButton']";
    this.cityInput = "//input[@id='address_TownOrCity']";

  }

    

}

export default AddAddressPage;