import AbtractsPage from "../commons/AbtractsPage";

/// <reference types="Cypress" />
class ProductListingPage extends AbtractsPage {

  constructor() {
    super();
    this.dynamicItems = "//body[@class='desktop noTouch']/div[@id='content']/div/div[@class='reebonzListingPage']/div/div[@class='container']/div[@class='row']/div[@class='col-md-9 col-sm-8 listing theiaStickySidebar']/div[@class='theiaStickySidebar']/div[@class='productListContainer listingWrapper']/div/div[@class='row products js-productWrapper']/div[valueItem]";
    this.addToBasketBtn = "//button[@name='AddToBasket']";
    this.itemBasketCount = "//span[@class='cartCount js-cartCount']";
    this.checkoutPopupBtn = "//a[@class='btn btn-sm btn-block js-cartBtn']";
    

  }

  clickToValidProduct = (element) => {
    var i = 1;
    var loop = (i) => {
      cy.xpath(element.replace('valueItem', i)).click().then(() => {
        cy.xpath(this.addToBasketBtn).then($button => {
          if ($button.is(':visible')) {
            cy.xpath(this.addToBasketBtn).click()
            return;
          } else {
            cy.go('back');
            loop(++i);
          }
        })
      })
    }
    loop(i);

  }

}

export default ProductListingPage;