import AbtractsPage from "../commons/AbtractsPage";

class HomePage extends AbtractsPage {

  constructor(){
    super();
    this.loginBtn = "//a[@class='js-login']";
    this.registerBtn = "//a[@class='js-register']";
    this.firstNameInput = "//input[@placeholder='First Name']";
    this.lastNameInpput = "//input[@placeholder='Last Name']";
    this.emailInput = "//input[@placeholder='Email Address']";
    this.passWordInput = "//input[@id='password']";
    this.countrySelect = "//select[@id='country']";
    this.registerWithEmailBtn = "//input[@class='btn btn-sm js-submit']";
    this.menuMen = "//a[@id='subMenu_men']";
    this.bagMen = "//li[@class='menuLink js-menuLink open']//div[contains(@class,'dropdown-menu subMenu')]//div[@class='wrapper']//div[@class='subMenuContainer']//div[@class='row']//div[@class='col-lg-8 col-sm-8 subCategoryWrapper']//div[@class='relative']//div[@class='col-sm-3 static']//ul//li//a[contains(text(),'Bags')]";
    this.reebonzLogo = "//div[@class='logoWrapper']//a//div";

  }

    

}

export default HomePage;