/// <reference types="Cypress" />

//     getElementByXpath (element){
//         return cy.xpath(element)
//     }

//     getElementByCss (element){
//         return cy.get(element)
//     }



class AbtractsPage {
  constructor() {

    this.random = Math.random()
      .toString(36)
      .replace(/[^a-z]+/g, "")
      .substr(0, 7);

    this.randomEmail = "bi.nguyen+" + this.random + "@reebonz.com";
    this.password = "123456"
    this.singaporeCountry = "Singapore"

  }
  getElementByXpath = (element) => {
    return cy.xpath(element)
  };

  getElementByCss = (element) => {
    return cy.get(element)
  };


  getElementByXpathAndClick = (element) => {
    return cy.xpath(element).click()
  };

  selectDropdownValueByXpath = (element, value) => {
    return cy.xpath(element).select(value)
  };

  getElementByCssAndClick = (element) => {
    return cy.get(element).click()
  };

  showElement = (element) => {
    return cy.log(element)
  }

  checkElementVisibleByXpath = (element) => {
    if (cy.xpath(element).should('be.visible')) {
      return true
    } else {
      return false
    }
  }

  typeToElementByXpath = (element, value) => {
    return cy.xpath(element).type(value)
  }

  convertToNumber = (value) => {
    value = value.match(/\d\.*/g);
    value = value.join("");
    value = Number(value);
    return value;
  };

  hoverElementByXpath = (element) => {
    // return cy.xpath("//div[@id='overlay']").then($input => {
    //   if ($input.is(':hidden')) {
    //     cy.xpath(element).trigger('mouseover')
    //    }
    //   })
    return cy.xpath(element).trigger('mouseover',{force: true})
  }

  assertTextByXpath = (element,value) => {
    return cy.xpath(element).should('not.have.text', value)
  }

  assertUrlContains = (value) => {
    return cy.url({timeout:60000}).should('include',value)
  }
  
  checkElementVisibleAndType =(element, value) => {
    return cy.xpath(element).then($input => {
      if ($input.is(':visible')) {
        cy.xpath(element).type(value)
       }
      })
  }


  //   checkElementIsPresentAndConvertToNumber = async (elementWait) => {
  //     let isVisible = cy.get(elementWait)

  //     if (isVisible) {
  //       let result = await elementWait.getText();
  //       let value = 0;
  //       value = await this.convertToNumber(result);
  //       return value;
  //     } else {
  //       let value = 0;
  //       return value;
  //     }
  //   };
}


export default AbtractsPage;
