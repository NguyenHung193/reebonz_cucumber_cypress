/// <reference types="Cypress" />

beforeEach(function () {
  cy.log('Start testing..........')
  console.log('Start test')
})

afterEach(function () {
  cy.log('End testing..........')
  cy.clearLocalStorage()
  cy.clearCookies()
  cy.reload(true)
})





Given(/^I open reebonz site$/, function () {
  cy.visit("https://" + Cypress.env('env') + "-www.reebonz-dev.com/sg")
});

// Given(/^I open reebonz site on Staging$/, function () {
//     cy.visit(Cypress.env('staging'))
// });

