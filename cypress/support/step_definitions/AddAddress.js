/// <reference types="Cypress" />

import { Given } from "cypress-cucumber-preprocessor/steps";
import HomePage from "../PageObjects/HomePageObject";
import ProductListingPage from "../PageObjects/ProductListingPageObjects";
import AbtractsPage from "../commons/AbtractsPage";
import AddAddressPage from "../PageObjects/AddAddressPage";



  const homePage = new HomePage();
  const productListingPage = new ProductListingPage();
  const addAddressPage = new AddAddressPage();

  When(/^I click to add new address button$/, function () {
    addAddressPage.getElementByXpathAndClick(addAddressPage.addNewAddressBtn)
  });

  And(/^I enter fullname$/, function () {
    addAddressPage.checkElementVisibleAndType(addAddressPage.fullNameInput, "Bi Nguyen")
  });

  And(/^I enter address$/, function () {
    addAddressPage.checkElementVisibleAndType(addAddressPage.addressInput, "16 An Nhon 3")
  });

  And(/^I enter city if having$/, function () {
    addAddressPage.checkElementVisibleAndType(addAddressPage.cityInput, "Da Nang")
  });

  And(/^I enter postal code if having$/, function () {
    addAddressPage.checkElementVisibleAndType(addAddressPage.postalCodeInput,"550000")
  });

  And(/^I enter phonenumber$/, function () {
    addAddressPage.typeToElementByXpath(addAddressPage.phoneNumberInput, "0905495922")
  });

  And(/^I click to add address button$/, function () {
    addAddressPage.getElementByXpathAndClick(addAddressPage.addAddressBtn)
  });

  Then(/^I see address page$/, function () {
    addAddressPage.assertUrlContains('address')
  });

  And(/^I click to address popup$/, function () {
    addAddressPage.getElementByXpathAndClick(addAddressPage.addressPopup)
  });

  When(/^I hover profile name$/, function () {
    addAddressPage.hoverElementByXpath(addAddressPage.profileName)
  });







