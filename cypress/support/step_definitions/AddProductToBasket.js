/// <reference types="Cypress" />

import { Given } from "cypress-cucumber-preprocessor/steps";
import HomePage from "../PageObjects/HomePageObject";
import ProductListingPage from "../PageObjects/ProductListingPageObjects";
import AbtractsPage from "../commons/AbtractsPage";



  const homePage = new HomePage();
  const productListingPage = new ProductListingPage();

  When(/^I hover men menu$/, function () {
    homePage.hoverElementByXpath(homePage.menuMen)
  });

  When(/^I click to item and add to basket$/, function () {
    productListingPage.clickToValidProduct(productListingPage.dynamicItems)
  });

  Then(/^I see add product successful$/, function () {
    productListingPage.assertTextByXpath(productListingPage.itemBasketCount,"0")
  });

  And(/^I click to bag list$/, function () {
    homePage.getElementByXpathAndClick(homePage.bagMen)
  });

  Then(/^I see bag listing page$/, function () {
    productListingPage.assertUrlContains('bags')
  });

  When(/^I hover to basket$/, function () {
    productListingPage.hoverElementByXpath(productListingPage.itemBasketCount)
  });

  Then(/^I see checkout page$/, function () {
    productListingPage.assertUrlContains('checkout')
  });

  And(/^I click to Checkout button$/, function () {
    productListingPage.getElementByXpathAndClick(productListingPage.checkoutPopupBtn)
  });

  

  







