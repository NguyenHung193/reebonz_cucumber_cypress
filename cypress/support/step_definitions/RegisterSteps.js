/// <reference types="Cypress" />

import { Given } from "cypress-cucumber-preprocessor/steps";
import HomePage from "../PageObjects/HomePageObject";
import AddAddressPage from "../PageObjects/AddAddressPage";
import AbtractsPage from "../commons/AbtractsPage";
// import HomePageUI from "../../integration/PageUI/HomePageUI"



  const homePage = new HomePage();

  When(/^I click to Register button$/, function () {
    homePage.getElementByXpathAndClick(homePage.registerBtn)
  });

  Then(/^I see reebonz site$/, function () {
    assert.isOk(homePage.checkElementVisibleByXpath(homePage.reebonzLogo))
    // cy.getCookie('ARRAffinity').then(value => cy.log('log--a ', value.domain))
    cy.url().should('include','reebonz')
  });

  Then(/^I see register successful$/, function () {
  cy.url({timeout:60000}).should('include','signup')
  });

  And(/^I enter first name$/, function () {
    homePage.typeToElementByXpath(homePage.firstNameInput,"Bi")
  });

  And(/^I enter last name$/, function () {
    homePage.typeToElementByXpath(homePage.lastNameInpput,homePage.random)
  });

  And(/^I enter email$/, function () {
    const homePage = new HomePage();
    homePage.typeToElementByXpath(homePage.emailInput,homePage.randomEmail)
  });

  And(/^I enter password$/, function () {
    homePage.typeToElementByXpath(homePage.passWordInput,homePage.password)
  }); 
                                                                

  And(/^I enter (.+)$/, function (country) {
    homePage.selectDropdownValueByXpath(homePage.countrySelect,country)
  });

  And(/^I click to login button$/, function () {
    homePage.getElementByXpathAndClick(homePage.registerWithEmailBtn)
  });

 

  







