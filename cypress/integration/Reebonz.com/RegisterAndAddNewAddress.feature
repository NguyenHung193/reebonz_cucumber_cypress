Feature: The Reebonz

    Scenario Outline:  Register and add new address
    Given I open reebonz site
    Then I see reebonz site
    When I click to Register button
    And I enter first name
    And I enter last name
    And I enter email
    And I enter password
    And I enter <country>
    And I click to login button
    Then I see register successful
    When I hover profile name
    And I click to address popup
    Then I see address page
    When I click to add new address button
    And I enter fullname
    And I enter address
    And I enter city if having
    And I enter postal code if having
    And I enter phonenumber
    And I click to add address button
    

    Examples:
    | country | Header 2 | Header 3 |
    | Singapore  | Value 2  | Value 3  |
    | Hong Kong  | Value 2  | Value 3  |
    | Malaysia  | Value 2  | Value 3  |
    | Myanmar  | Value 2  | Value 3  |
    | New Zealand  | Value 2  | Value 3  |
    | Qatar  | Value 2  | Value 3  |



