Feature: The Reebonz

  Scenario Outline:  Checkout bag item <country>
    Given I open reebonz site
    Then I see reebonz site
    When I click to Register button
    And I enter first name
    And I enter last name
    And I enter email
    And I enter password
    And I enter <country>
    And I click to login button
    Then I see register successful
    When I hover men menu
    And I click to bag list
    Then I see bag listing page
    When I click to item and add to basket
    Then I see add product successful
    When I hover to basket
    And I click to Checkout button
    Then I see checkout page

    Examples:
      | country     | Header 2 | Header 3 |
      | Singapore   | Value 2  | Value 3  |
      | Hong Kong   | Value 2  | Value 3  |
      | Malaysia    | Value 2  | Value 3  |
      | Myanmar     | Value 2  | Value 3  |
      | New Zealand | Value 2  | Value 3  |
      | Qatar       | Value 2  | Value 3  |



